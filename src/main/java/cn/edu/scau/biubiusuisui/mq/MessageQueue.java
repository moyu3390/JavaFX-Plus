package cn.edu.scau.biubiusuisui.mq;

import cn.edu.scau.biubiusuisui.annotation.FXReceiver;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import cn.edu.scau.biubiusuisui.entity.FXMethodEntity;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author jack
 * @version 1.0
 * @date 2019/6/25 12:24
 * @since JavaFX2.0 JDK1.8
 */
public class MessageQueue {
    private static final IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(MessageQueue.class);

    private static Map<String, List<FXMethodEntity>> receivers = new ConcurrentHashMap<>();  //Map<主题，订阅了主题的所有方法>

    private static MessageQueue messageQueue = null;

    private MessageQueue() {
    }

    /**
     * 获取mq单例
     *
     * @return MessageQueue
     */
    public static synchronized MessageQueue getInstance() {
        if (messageQueue == null) {
            messageQueue = new MessageQueue();
        }
        return messageQueue;
    }

    /**
     * @param fxBaseController      基础controller
     * @param fxBaseControllerProxy 基础controller代理
     * @description 注册消费者，即FXReceiver注解的method
     */
    public void registerConsumer(FXBaseController fxBaseController, FXBaseController fxBaseControllerProxy) {
        Class clazz = fxBaseController.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (FXReceiver.class.equals(annotation.annotationType())) {
                    logger.info("registering consumer: " + fxBaseControllerProxy.getName());
                    FXReceiver receiver = (FXReceiver) annotation;
                    FXMethodEntity fxMethodEntity = new FXMethodEntity(fxBaseControllerProxy, method);
                    List<FXMethodEntity> fxMethodEntities = receivers.get(receiver.name());
                    if (fxMethodEntities == null) {
                        fxMethodEntities = new ArrayList<>();
                    }
                    fxMethodEntities.add(fxMethodEntity);
                    receivers.put(receiver.name(), fxMethodEntities);
                }
            }
        }
    }

    /**
     * @param id  消息topic
     * @param msg 消息内容
     * @description 处理消息发送
     */
    public void sendMsg(String id, Object msg) {
        List<FXMethodEntity> lists = receivers.get(id);
        if (lists != null) {
            for (FXMethodEntity fxMethodEntity : lists) {
                Method method = fxMethodEntity.getMethod();
                method.setAccessible(true);
                FXBaseController fxBaseController = fxMethodEntity.getFxBaseController();
                if (method.getParameterCount() == 0) {
                    try {
                        method.invoke(fxBaseController);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        logger.error(e.getMessage());
                    }
                } else {
                    try {
                        // 调起FXReceiver注解的方法
                        method.invoke(fxBaseController, msg);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        }
    }
}
