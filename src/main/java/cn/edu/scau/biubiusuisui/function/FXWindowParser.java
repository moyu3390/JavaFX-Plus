package cn.edu.scau.biubiusuisui.function;

import cn.edu.scau.biubiusuisui.annotation.FXWindow;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import cn.edu.scau.biubiusuisui.exception.ProtocolNotSupport;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;
import cn.edu.scau.biubiusuisui.utils.FileUtil;
import cn.edu.scau.biubiusuisui.utils.StringUtil;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

/**
 * @author jack
 * @version 1.0
 * @date 2019/6/30 10:40
 * @description 解析@FXWindow
 * @since JavaFX2.0 JDK1.8
 */
public class FXWindowParser {
    private static final IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(FXWindowParser.class);

    public void parse(Stage stage, FXBaseController fxControllerProxy, FXWindow fxWindow) {
        logger.info("parsing @FXWindow of class: " + fxControllerProxy.getName());

        // 处理 title
        fxControllerProxy.setWindowTitle(fxWindow.title());

        // 处理 icon
        fxControllerProxy.setIcon(fxWindow.icon());

        // 处理draggable和resizable
        if (fxWindow.draggable() || fxWindow.resizable()) {
            fxControllerProxy.setDragAndResize(fxWindow.draggable(), fxWindow.resizable());
        }

        // fxWindow的resizable默认为false
        if (fxWindow.resizable()) {
            fxControllerProxy.setDragAndResize(fxWindow.draggable(), true);
        }


        // 处理style
        stage.initStyle(fxWindow.style());
    }
}
