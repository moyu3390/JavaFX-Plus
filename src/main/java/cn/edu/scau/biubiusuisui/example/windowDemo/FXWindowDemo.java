package cn.edu.scau.biubiusuisui.example.windowDemo;

import cn.edu.scau.biubiusuisui.annotation.FXScan;
import cn.edu.scau.biubiusuisui.config.FXPlusApplication;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author suisui
 * @description 测试开放设置部分FXWindow属性的接口
 * @date 2020/8/29 09:40
 * @since JDK1.8
 */
@FXScan(base = "cn.edu.scau.biubiusuisui.example.windowDemo")
public class FXWindowDemo extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXPlusApplication.start(FXWindowDemo.class);
    }
}
